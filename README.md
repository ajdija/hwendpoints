======================
Instalation Instructions
======================

Your development environment will require:
*  Typesafe Activator
*  Sbt
*  Scala

To download other dependencies type:
* ** sbt run

and finally to run app and create endpoints (localhost/invitation):
* ** ./activator ui

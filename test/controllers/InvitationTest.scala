package controllers

import java.util.concurrent.TimeUnit

import org.specs2.mutable._
import play.api.libs.json.Json

import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration

/**
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class InvitationTest extends Specification {

  val timeout: FiniteDuration = FiniteDuration(5, TimeUnit.SECONDS)

  "Invitation" should {

    "send 404 on a bad request" in {
      running(FakeApplication()) {
        route(FakeRequest(GET, "/boum")) must beNone
      }
    }

    "POST /invitation" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(POST, "/invitation").withJsonBody(Json.obj(
          "invitee" -> "John Smith",
          "email" -> "john@smith.mx"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }

    "GET /invitation" in {
      running(FakeApplication()) {
        val home = route(FakeRequest(GET, "/invitation")).get
        status(home) must equalTo(200)
        contentType(home) must beSome.which(_ == "application/json")
      }
    }

  }

}
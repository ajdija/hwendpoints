package controllers

import org.slf4j.{LoggerFactory, Logger}
import play.api.mvc._
import models._
import models.JsonFormats._
import play.api.libs.json.Json
import scala.concurrent.Future

/**
 * Created by Mateusz Grzesiukiewicz on 09.03.2015.
 */
class Invitation extends Controller {

  def receiveInvitation = Action.async(parse.json) {
    request =>
      request.body.validate[User].map {
        user =>
          Future.successful(Created(s"Hi John"))
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def showInvitation = Action {
    Ok(Json.toJson(User("John Smith", "john@smith.mx")))
  }
}
